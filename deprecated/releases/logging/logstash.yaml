apiVersion: helm.fluxcd.io/v1
kind: HelmRelease
metadata:
  name: logstash
  namespace: monitoring
  annotations:
    flux.weave.works/automated: "false"
spec:
  releaseName: logstash
  chart:
    repository: https://helm.elastic.co
    name: logstash
    version: 7.10.1
  values:
    imageTag: 7.10.1

    persistence:
      enabled: false
    service:
      type: ClusterIP
      ports:
        - port: 5044
          name: logstash-service
          targetPort: 5044

    logstashPipeline:
      logstash.conf: |
        input {
          beats {
            port => 5044
          }
        }

        filter {
          mutate {
            add_field => { "producer" => "escape_wp2"}
            add_field => { "type" => "k8s-cluster"}
          }
          grok {
            match => {"[kubernetes][node][name]" => '(?<cluster>(?:[()a-z]+-[()a-z]+-[()a-z]+-[()0-9]+))'}
          }
          ruby {
            code => "event.set('timestamp',(event.get('@timestamp').to_f * 1000).to_i)"
          }
          if [message] =~ "oracle:" or [message] =~ "mysql+pymysqll:" or [message] =~ "mysql:" or [message] =~ "postgres:" {   # Don't let secrets leak
            drop { }
          } else if [kubernetes][labels][app] =~ "rucio-server" {
            mutate { add_field => {"type" => "server"} }
            mutate { remove_field => [ "host" ] } 
            if [message] =~ "kube-probe" or [message] =~ "health-check" {
              drop { }
            }
            if [stream] == "stdout" {
              grok {
                match => {"message" => '%{DATA}\t%{DATA:frontend}\t%{DATA:backend}\t%{DATA:clientip}\t%{DATA:requestid}\t%{NUMBER:status}\t%{NUMBER:bytesinput}\t%{NUMBER:bytesoutput}\t%{NUMBER:duration}\t"%{WORD:method} %{URIPATHPARAM:request} %{DATA:http}"\t"%{DATA:token}"\t"%{DATA:ua}"\t%{GREEDYDATA:scriptid}'}
                match => {"message" => '%{DATA}\t%{DATA:frontend}\t%{DATA:backend}\t%{DATA:clientip}\t%{DATA:requestid}\t%{NUMBER:status}\t%{NUMBER:bytesinput}\t%{NUMBER:bytesoutput}\t%{NUMBER:duration}\t"%{WORD:method} %{URIPROTO}://%{URIHOST}%{URIPATHPARAM:request} %{DATA:http}"\t"%{DATA:token}"\t"%{DATA:ua}"\t%{GREEDYDATA:scriptid}'}
                tag_on_failure => "_failed_parsing_restcall"
              }

              if !("_failed_parsing_restcall" in [tags]) {  # A REST-API call was executed

                grok {
                  match => { "token" => "%{WORD:account}-%{GREEDYDATA:username}-%{DATA}" }
                  remove_field => [ "token" ]
                }

                grok {
                  match => { "ua" => "%{DATA:[useragent][name]}/%{NOTSPACE:[useragent][version]}" }
                  remove_field => [ "ua" ]
                  tag_on_failure => "_failed_parsing_useragent"
                }

                if "_failed_parsing_useragent" in [tags] {  # In case the grok above didn't match, the raw useragent string from the log is used
                  mutate {
                    add_field => { "[useragent][name]" => "%{ua}" }
                    remove_field => [ "ua" ]
                    remove_tag => [ "_failed_parsing_useragent" ]
                  }
                }

                if [clientip] =~ '\,\s' { mutate { gsub => ["clientip", ",\s.*$", ""] } } # Split client IP in case of proxy-forward (e.g. webui)

              }
            }
          } else if [kubernetes][labels][app] == "rucio-daemons" {
            mutate { add_field => {"type" => "daemons"} }
            mutate { add_field => { "application" => "%{[kubernetes][container][name]}" }}
            if [stream] == "stdout" {
              grok {
                #match => { "message" => "^(?<ts>\d{4}-\d{2}-\d{2}\s\d{2}:\d{2}:\d{2},\d{3})\t%{NUMBER:pid}\t%{LOGLEVEL:severity_label}\t%{GREEDYDATA:message}" }
                match => { "message" => "^(?<ts>\d{4}-\d{2}-\d{2}\s\d{2}:\d{2}:\d{2},\d{3})\t%{NOTSPACE:loggername}\t%{NUMBER:pid}\t%{LOGLEVEL:severity_label}\t%{GREEDYDATA:message}" }
                overwrite => ["message", "severity_label", "ts"]
                tag_on_failure => "_failed_parsing_ruciolog"
              }
            }
          } else {
            mutate {add_field => {"type" => "generic"}}
          }
        }

        output {
          # Send message in JSON form to CERN monit
          http {
            url => "http://monit-logs.cern.ch:10012/"
            format => "json"
            http_method => "post"
            id => "monit_http_output"
          }
        }
